﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_Num1_Num2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num1;
            int[] num2;
            int lengthNum1;
            int lengthNum2;
            int lengthMax;
            int temp;
            int[] tempMas;
            int[] sumMas;
            bool dec = false;   // Метка, для случаев, когда при сложении переносится единица

            Console.Write("Введите длину первого слагаемого: ");
            lengthNum1 = int.Parse(Console.ReadLine());
            num1 = new int[lengthNum1];

            Console.Write("Введите длину второго слагаемого: ");
            lengthNum2 = int.Parse(Console.ReadLine());
            num2 = new int[lengthNum2];

            Console.WriteLine("Введите первое слагаемое: ");
            for (int i = 0; i < lengthNum1; i++)
            {
                num1[i] = int.Parse(Console.ReadLine());
            }
            Console.WriteLine("Введите второе слагаемое: ");
            for (int i = 0; i < lengthNum2; i++)
            {
                num2[i] = int.Parse(Console.ReadLine());
            }

            // Реверс первого числа
            for (int i = 0; i < lengthNum1 / 2; i++)
            {
                temp = num1[i];
                num1[i] = num1[lengthNum1 - 1 - i];
                num1[lengthNum1 - 1 - i] = temp;
            }

            // Реверс второго числа
            for (int i = 0; i < lengthNum2 / 2; i++)
            {
                temp = num2[i];
                num2[i] = num2[lengthNum2 - 1 - i];
                num2[lengthNum2 - 1 - i] = temp;
            }

            // Определение самого длинного числа
            if (lengthNum1 > lengthNum2)
            {
                lengthMax = lengthNum1 + 1;
            }
            else
            {
                lengthMax = lengthNum2 + 1;
            }


            // Приводим массивы к одной длине
            tempMas = new int[lengthMax];

            for (int i = 0; i < lengthNum1; i++)
            {
                tempMas[i] = num1[i];
            }
            for (int i = lengthNum1; i < lengthMax; i++)
            {
                tempMas[i] = 0;
            }

            num1 = new int[lengthMax];
            for (int i = 0; i < lengthMax; i++)
            {
                num1[i] = tempMas[i];
            }
            for (int i = 0; i < lengthNum2; i++)
            {
                tempMas[i] = num2[i];
            }
            for (int i = lengthNum2; i < lengthMax; i++)
            {
                tempMas[i] = 0;
            }

            num2 = new int[lengthMax];
            for (int i = 0; i < lengthMax; i++)
            {
                num2[i] = tempMas[i];
            }

            // Сложение
            sumMas = new int[lengthMax];
            for (int i = 0; i < lengthMax; i++)
            {
                dec = false;
                sumMas[i] = num1[i] + num2[i];
                if (sumMas[i] >= 10)
                {
                    sumMas[i] = sumMas[i] % 10;
                    dec = true;
                }
                if (dec == true)
                {
                    num1[i+1]++;
                }
            }

            // Обратный реверс суммы чисел
            for (int i = 0; i < lengthMax / 2; i++)
            {
                temp = sumMas[i];
                sumMas[i] = sumMas[lengthMax - 1 - i];
                sumMas[lengthMax - 1 - i] = temp;
            }

            // Убираем лишний ноль перед числом и выводим на экран
            if (sumMas[0] == 0)
            {
                int[] tempMas2 = new int[lengthMax - 1];
                for(int i = 0; i < lengthMax - 1; i++)
                {
                    tempMas2[i] = sumMas[i + 1];
                }
                sumMas = new int[lengthMax - 1];
                for (int i = 0; i < lengthMax - 1; i++)
                {
                    sumMas[i] = tempMas2[i];
                }
                Console.WriteLine("Результат сложения: ");
                for (int i = 0; i < lengthMax - 1; i++)
                {
                    Console.Write(sumMas[i]);
                }
            }
            else
            {
                Console.WriteLine("Результат сложения: ");
                for (int i = 0; i < lengthMax; i++)
                {
                    Console.Write(sumMas[i]);
                }
            }
            
            Console.ReadKey();
        }
    }
}
